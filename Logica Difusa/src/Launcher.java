
/**
 *
 * @author Juan Esteban Arias
 * @author Christian Jimenez
 * @author Camilo Hernandez
 */
public class Launcher {
    
    public static void main(String[] args) {
        
        new Presentacion().principal();
        
    }
}
