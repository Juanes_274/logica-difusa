
import java.util.Scanner;

public class Presentacion {

    public Scanner leerDato = new Scanner(System.in);

    public void principal() {

        int opc;
        boolean salir = false;
        while (!salir) {
            System.out.println("\n\n***_____Menu de opciones_____***\n");
            System.out.println("1. Ingresar altura");
            System.out.println("2. Salir\n");
            System.out.print("Ingrese una opcion: ");
            opc = leerDato.nextInt();
            switch (opc) {
                case 1:
                    ingresarAltura();
                    break;

                case 2:
                    salir = true;
                    break;

                default:
                    System.out.println("!!!OPCION INCORRECTA!!!");
                    break;
            }
        }
    }

    public void ingresarAltura() {
        System.out.print("Ingrese la altura de la persona: ");
        int altura = leerDato.nextInt();

        logicaDifusa(altura);

    }

    public void logicaDifusa(int a) {
        System.out.println(".................");
        System.out.println("R E S U L T A D O");
        System.out.println(".................");
        
        if (a > 210) {
            System.out.println("Usted es Muy alto");
        } else if (a > 175 && a < 211) {
            System.out.println("Usted es Alto");
        } else if (a > 120 && a < 176) {
            System.out.println("Usted es Mediano");
        } else if (a > 90 && a < 177) {
            System.out.println("Usted es Bajo");
        } else if (a < 91) {
            System.out.println("Usted es Muy Bajo");
        }
    }
}
